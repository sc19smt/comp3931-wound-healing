import os
from re import X
import tensorflow as tf
from tensorflow import keras
import cv2
from glob import glob
import numpy as np
import matplotlib.pyplot as plt
from PIL import Image

IMAGE_SIZE = 256
PATH = "personal_dataset/"

smooth = 1e-15

def dice_coef(y_true, y_pred):
    y_true = tf.keras.layers.Flatten()(y_true)
    y_pred = tf.keras.layers.Flatten()(y_pred)
    intersection = tf.reduce_sum(y_true * y_pred)
    return (2. * intersection + smooth) / (tf.reduce_sum(y_true) + tf.reduce_sum(y_pred) + smooth)

def dice_loss(y_true, y_pred):
    return 1.0 - dice_coef(y_true, y_pred)

new_model = tf.keras.models.load_model('saved_model/my_model', custom_objects={'dice_coef': dice_coef, 'dice_loss': dice_loss})

images = sorted(glob(os.path.join(PATH, "5/*")), key=lambda f: int(''.join(filter(str.isdigit, f))))

test_x = images

def read_image(path):
    x = cv2.imread(path, cv2.IMREAD_COLOR)
    x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    x = cv2.resize(x, (IMAGE_SIZE, IMAGE_SIZE))
    x = x/255.0
    return x

def mask_parse(mask):
    mask = np.squeeze(mask)
    mask = [mask, mask, mask]
    mask = np.transpose(mask, (1, 2, 0))
    return mask

i = 0
for x in test_x:
    i = i + 1
    x = read_image(x)
    y_pred = new_model.predict(np.expand_dims(x, axis=0))[0] > 0.5
    h, w, _ = x.shape
    black_line = np.zeros((h, 10, 3))
    all_images = [
        mask_parse(y_pred), black_line
    ]
    image = np.concatenate(all_images, axis=1)
    
    fig = plt.figure(figsize=(12, 12))
    a = fig.add_subplot(1, 1, 1)
    imgplot = plt.imshow(image)
    plt.axis('off')
    plt.margins(0,0)
    plt.gca().xaxis.set_major_locator(plt.NullLocator())
    plt.gca().yaxis.set_major_locator(plt.NullLocator())
    plt.savefig(PATH + "5_mask/" + str(i) + '.png', bbox_inches='tight', pad_inches=0)
    plt.show()


def read_image2(path):
    x = cv2.imread(path, cv2.IMREAD_COLOR)
    x = cv2.cvtColor(x, cv2.COLOR_BGR2RGB)
    return x

masks = sorted(glob(os.path.join(PATH, "5_mask/*")), key=lambda f: int(''.join(filter(str.isdigit, f))))

healing = []
for x in masks:
    x = read_image2(x)
    white_pix = np.sum(x == 255)
    healing.append(white_pix)

print (healing)

for i in range(len(healing) - 1):
    j = i + 1
    if (i <= 3):
        if (healing[i] == healing[j] or healing[i] > healing[j]):
            print("Wound seems to be healing well. Please check for odours and exudates.")
        else:
            print("Wound might not be healing well 2.")
    else:
        if (healing[i] > healing[j]):
            print("Wound seems to healing well.")
        elif (healing[i] <= 100 or healing[j] <= 100):
            print("Wound seems to be healing well.")
        elif (healing[i] < healing[j]):
            print("Wound might not be healing well")  
